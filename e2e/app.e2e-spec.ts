import { ExemplePage } from './app.po';

describe('exemple App', function() {
  let page: ExemplePage;

  beforeEach(() => {
    page = new ExemplePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
