/**
 * Created by souleymane91 on 2/8/17.
 */
import {Directive, ElementRef} from "@angular/core";

@Directive({
  selector:'[autoGrow]',
  host:{
    '(focus)': 'onFocus()',
    '(blur)': 'onBlur()'
  }
})
export class AutoGrowDirective{
  constructor(private el: ElementRef){}

  onFocus(){
    this.el.nativeElement.style.backgroundColor = '200px';
  }

  onBlur(){
    this.el.nativeElement.style.width = '120px';
  }
}
