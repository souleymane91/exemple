import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import {AppComponent} from './app.component';
import {AutoGrowDirective} from "./exemple.directive";
import {Routes, RouterModule} from "@angular/router";
import { UserComponent } from './user/user.component';
import { UserDetailComponent } from './user/user-detail/user-detail.component';
import { UserListComponent } from './user/user-list/user-list.component';
import { UserAddComponent } from './user/user-add/user-add.component';

const appRoutes: Routes = [
  {path: 'users', component: UserComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    AutoGrowDirective,
    UserComponent,
    UserDetailComponent,
    UserListComponent,
    UserAddComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
