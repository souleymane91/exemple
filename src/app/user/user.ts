export class User {
  private _login: string;
  private _email: string;

  constructor(){}


  get login(): string {
    return this._login;
  }

  set login(value: string) {
    this._login = value;
  }


  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }
}
