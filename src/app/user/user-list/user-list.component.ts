import { Component, OnInit } from '@angular/core';
import {User} from "../user";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html'
})
export class UserListComponent implements OnInit {
  users: User[] = new Array();

  constructor() {
    var user1 = new User();
    var user2 = new User();

    user1.login = "user1Login";
    user1.email = "user1@gmail.com";
    user2.login = "user2Login";
    user2.email = "user2@gmail.com";

    this.users.push(user1);
    this.users.push(user2);
  }

  ngOnInit() {
  }

}
